Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xkbcommon
Upstream-Contact: Remi Thebault <remi.thebault@gmail.com>
Source: https://github.com/rust-x-bindings/xkbcommon-rs

Files: *
Copyright: 2016-2019 Remi Thebault <remi.thebault@gmail.com>
License: MIT

Files: src/xkb/keysyms.rs
Copyright:
 1987, 1988, 1991, 1994, 1998  The Open Group
 1987, 1988 Digital Equipment Corporation, Maynard, Massachusetts
 1991 Oracle and/or its affiliates
 2016 Remi Thebault <remi.thebault@gmail.com>
License: MIT

Files: debian/*
Copyright:
 2020-2023 Debian Rust Maintainers <pkg-rust-maintainers@alioth-lists.debian.net>
 2020-2023 Arnaud Ferraris <aferraris@debian.org>
License: MIT

License: MIT
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
